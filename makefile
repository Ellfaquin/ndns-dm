# Component
COMPONENT = x_nextdns

# Directories
DESTDIR = bin
DESTDIR_SO = /usr/lib/amx/$(COMPONENT)
DESTDIR_ODL = /etc/amx/$(COMPONENT)
DESTDIR_SCRIPTS = /usr/scripts/$(COMPONENT)
BINDIR = /usr/bin
SRCDIR = src
ODLDIR = odl
INCDIR = inc
OBJDIR = obj
SCRIPTSDIR= scripts

# Files
SRCS = $(wildcard $(SRCDIR)/*.c)
OBJECTS=$(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))
ODLS = $(wildcard $(ODLDIR)/*.odl)
TARGET_SO = $(DESTDIR)/$(COMPONENT).so
SCRIPTS = $(wildcard $(SCRIPTSDIR)/*.sh)

# Compiler options
CFLAGS += -Wall -Werror -fPIC -I${INCDIR}
LDFLAGS_C += -shared \
		   -lamxb -lamxc -lamxd -lamxo -lamxp

#TARGET_BIN = fti_encode

install: all $(CGIS)
	install -d -m 0755 $(DEST)$(DESTDIR_SO)
	install -D -p -m 0755 $(TARGET_SO) $(DEST)$(DESTDIR_SO)/$(COMPONENT).so
	install -d -m 0755 ${DEST}${BINDIR}
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	install -d -m 0755 $(DEST)$(DESTDIR_ODL)
	install -D -p -m 0644 odl/*.odl $(DEST)$(DESTDIR_ODL)/
	install -d -m 0755 $(DEST)$(DESTDIR_SCRIPTS)
	install -D -p -m 0644 $(SCRIPTS) $(DEST)$(DESTDIR_SCRIPTS)/

all: $(TARGET_SO)


$(TARGET_SO): $(OBJECTS) $(DESTDIR)
	$(CC) $(CFLAGS) -Wl,-soname,$(COMPONENT).so -o $@ $(OBJECTS) $(LDFLAGS) $(LDFLAGS_C)

$(OBJDIR)/%.o: $(SRCDIR)/%.c $(OBJDIR)
	$(CC) $(CFLAGS) -c $< -o $@ $(LDFLAGS) $(LDFLAGS_C)
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(DESTDIR):
	mkdir -p $@

$(OBJDIR):
	mkdir -p $@

clean:
	rm -f $(TARGET_SO)
	rm -f $(OBJECTS)
	rm -f $(wildcard $(SRCDIR)/*.d)
	rm -rf $(DEST)$(DESTDIR_SO)/*
	rm -f $(DEST)$(DESTDIR_ODL)/*
	rm -f $(DEST)$(BINDIR)/$(COMPONENT)
	rm -f $(DEST)$(DESTDIR_SO)/$(COMPONENT).so

.PHONY: all clean install_tests build_tests
