#!/bin/sh
case $1 in
    start)
        service nextdns restart
        ;;
    stop)
        service nextdns stop
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    *)
        echo "Usage : $0 [start|stop]"
        ;;
esac