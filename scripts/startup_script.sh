#!/bin/sh
case $1 in
    start)
        echo "______________________" > log_startup
        echo "___ HL-API DNS Forwarding Config." >> log_startup
        controller-container 'Device.DNS.Relay.Forwarding.3.DNSServer="192.168.1.1@54"' 1>>log_startup 2>>log_startup
        controller-container 'Device.DNS.Relay.Forwarding.5.Enable=0' 1>>log_startup 2>>log_startup

        echo "___ NextDNS Config and Launching." >> log_startup
        nextdns config set -setup-router=false 1>>log_startup 2>>log_startup
        nextdns config set -profile=f25b6b
        sed -i '/^listen/d' /etc/nextdns.conf
        echo "listen 192.168.1.1:54" >> /etc/nextdns.conf
        if ! [ -f /etc/rc2.d/S50nextdns ]; then
                echo "STARTUP SCRIPT - NextDNS is not installed. Installing." >> log_startup
                nextdns install 2>>log_startup 1>>log_startup
                echo "STARTUP SCRIPT - NextDNS is now installed. Starting." >> log_startup
        else
                echo "STARTUP SCRIPT - NextDNS is already installed. Starting." >> log_startup
        fi
        service nextdns restart 1>>log_startup 2>>log_startup
        x_nextdns &
        ;;
    stop)
        echo "TODO define behavior"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    *)
        echo "Usage : $0 [start|stop]"
        ;;
esac