#if !defined(__SCRIPTS_MANAGER_H__)
#define __SCRIPTS_MANAGER_H__
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <stdio.h>

/**
 * @brief Starts the NextDNS CLI.
 * 
 * @return int Exit status of the script.
 */
int nextdns_cli_start();

/**
 * @brief Stops the NextDNS CLI.
 * 
 * @return int Exit status of the script.
 */
int nextdns_cli_stop();

#ifdef __cplusplus
}
#endif