#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <stdio.h>

// Ambiorix variant types
#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>

#include <amxp/amxp.h>

// For actions on the datamodel
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
 
// Necessary to save and load the datamodel
#include <amxo/amxo.h>
#include <amxo/amxo_save.h>

#define NEXTDNS_DM_SAVE_FILE_PATH "/usr/nextdns.save"

// Custom functions
#include "client_config_manager.h"
#include "scripts_manager.h"

int _nextdns_main(int reason,
                    amxd_dm_t* dm,
                    amxo_parser_t* parser);


amxd_dm_t *nextdns_get_dm(void);
amxo_parser_t *nextdns_get_parser(void);

#ifdef __cplusplus
}
#endif