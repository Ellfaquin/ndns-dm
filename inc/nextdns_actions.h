#ifdef __cplusplus
extern "C"
{
#endif

#include "nextdns.h"

amxd_status_t _dummy_action(amxd_object_t* object,
                       amxd_param_t* param,
                       amxd_action_t reason,
                       const amxc_var_t* const args,
                       amxc_var_t* const retval,
                       void* priv);

#ifdef __cplusplus
}
#endif
