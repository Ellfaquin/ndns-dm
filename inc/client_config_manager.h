#ifndef CLIENT_CONFIG_MANAGER_H
#define CLIENT_CONFIG_MANAGER_H

#ifdef __cplusplus
extern "C"
{
#endif


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nextdns_utils.h"

extern const char *NEXTDNS_CONF_PATH_FILE;
extern const int NEXTDNS_CONF_MAX_LINE_LENGTH;
extern const int NEXTDNS_CONF_MAX_LINE_COUNT;
extern const size_t NEXTDNS_CONF_MAX_PROFILES_COUNT;
extern const size_t NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT;
extern const size_t NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT; // IPv6 : 39 characters


/**
 * @brief One conditional NextDNS profile. Both parameters should be non-null.
 */
typedef struct profile_t profile_t;

struct profile_t{
    char *target;
    char *profile_id;
    profile_t *next;
};

typedef struct {
    int count;
    profile_t *first;
    char *default_profile_id;
}profile_list_t;

extern profile_list_t profiles;

typedef enum {
    ccm_status_ok,
    ccm_error_io,
    ccm_error_empty_argument,
    ccm_error_invalid_argument,
    ccm_error_memory,
    ccm_error_uninitialized,
    ccm_status_not_found,
    ccm_error_empty_list
}ccm_status_t;

/**
 * @brief Initializes the "profiles" variables.
 * 
 * @param ret String for additional return status. Caller responsible for memory allocation & freeing.
 * @return "ccm_status_ok" if OK, "ccm_error_memory" if profiles memory allocation fails.
 */
ccm_status_t ccm_initialize(char *ret);

/**
 * @brief Sets the "profile" parameter of nextdns.conf.
 * 
 * @param profile_id expecting <10 chars in length.
 * @param ret        Returns the profile id if successful.
 * 
 * @return  "ccm_status_ok" if OK, "ccm_error_empty_argument" or "ccm_error_invalid_argument" if wrong "profile_id" argument,
 *          "ccm_error_uninitialized" if the initialize function was not called before.
 */
ccm_status_t ccm_set_default_profile(const char *profile_id, char *ret);

/**
 * @brief Adds a new conditional profile to the conf.
 * 
 * @param target        MAC (e.g. 00:1c:42:2e:60:4a) or IPv4/v6 (e.g. 10.0.4.0/24) of the target device / subnet, expecting <40 char in length.
 * @param profile_id    NextDNS profile ID, expecting <10 char in length.
 * 
 * @return  "ccm_status_ok" if OK, "ccm_error_empty_argument" or "ccm_error_invalid_argument" if wrong argument,
 *          "ccm_error_memory" if profiles memory allocation fails, "ccm_error_uninitialized" if the initialize function was not called before.
 */
ccm_status_t ccm_add_profile(const char *profile_id, const char *target, char *ret);

/**
 * @brief Removes a profile from the conf.
 * The target is used because profile_ids are not unique keys.
 * @param target MAC address or IPv4/v6 subnet / address. Expecting <40 char in length
 * 
 * @return  "ccm_status_ok" if OK, "ccm_error_empty_list" if the profiles list is empty, "ccm_error_empty_argument" or "ccm_error_invalid_argument" if wrong argument.
 */
ccm_status_t ccm_remove_profile(const char *target, char *ret);

/**
 * @brief Synchronizes the "profiles" variable with the nextdns.conf file.
 * 
 * @return  "ccm_status_ok" if OK, "ccm_error_io" if can't handle the file management, 
 *          "ccm_error_empty_list" if there is no default profile, "ccm_error_uninitialized" if the initialize function was not called before.
 */
ccm_status_t ccm_synchronize_file(char *ret);

/**
 * @brief Frees the memory of a "profile_t" variable.
 * 
 * @param profile Profile to free.
 */
void ccm_free_profile(profile_t *profile);

/**
 * @brief Removes all "profile_t" objects from the profiles list and frees their memory.
 * 
 * @return ccm_status_t 
 */
ccm_status_t ccm_flush_profiles();

/**
 * @brief Creates the profile struct from what is in the configuration file.
 * 
 * @return "ccm_status_ok" if OK, "ccm_error_io" if can't handle the file management, 
 *         "ccm_error_uninitialized" if the initialize function was not called before.
 */
ccm_status_t ccm_get_from_conf_file(char *ret);

#ifdef __cplusplus
}
#endif

#endif