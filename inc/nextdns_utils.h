#ifdef __cplusplus
extern "C"
{
#endif

#include "nextdns.h"

/**
 * @brief Retrieves parameters (depth 1) of the NextDNS data model.
 */
amxd_status_t nextdns_get_params(amxc_var_t *params);

/**
 * @brief Retrieves a list of the NextDNS data model.
 */
amxd_object_t* nextdns_get_profiles();

/**
 * @brief Retrieves the amxd_object_t value of the NextDNS data model.
 * 
 * @return amxd_object_t* NextDNS data model object.
 */
amxd_object_t* nextdns_get_object();

/**
 * @brief Retrieves the number of instances of the Profiles object in the NextDNS data model.
 * 
 * @return uint32_t 
 */
uint32_t nextdns_get_profile_count();

/**
 * @brief Adds an instance of the Profiles object to the NextDNS data model.
 * 
 * @param profile_id    
 * @param target        IPv4/v6 or MAC address
 */
void nextdns_add_profiles_instance(char *profile_id, char *target);

/**
 * @brief Sets the DefaultProfileId parameter of the NextDNS data model.
 * 
 * @param profile_id 
 */
void nextdns_set_default_profile_dm(char *profile_id);

/**
 * @brief Removes all Profiles instances from the NextDNS data model.
 * 
 */
void nextdns_remove_all_profiles_instances();

#ifdef __cplusplus
}
#endif

