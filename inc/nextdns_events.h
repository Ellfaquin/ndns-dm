#ifdef __cplusplus
extern "C"
{
#endif

#include "nextdns.h"
#include "scripts_manager.h"

void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv);

/**
 * @brief Triggers when the URL parameter of the NextDNS data model is changed.
 */
void _on_enable_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv );

/**
 * @brief Triggers when the DefaultProfileId parameter of the NextDNS data model is changed.
 */
void _on_default_profile_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv );

/**
 * @brief Triggers when a new instance of the "NextDNS.Profiles." data model object is added.
 */
void _on_profile_added(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv );

/**
 * @brief Triggers when any parameter of any instance of the "NextDNS.Profiles." data model object is modified.
 */
void _on_profile_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv );
                  
/**
 * @brief Updates the profiles list (data model) according to the client_config_manager status.
 * 
 */
void update_profiles_list();

#ifdef __cplusplus
}
#endif

