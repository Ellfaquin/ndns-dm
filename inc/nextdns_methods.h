#ifndef NEXTDNS_METHODS_H
#define NEXTDNS_METHODS_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "nextdns.h"
#include "nextdns_utils.h"

amxd_status_t dummy(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _NextDNS_initialize(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _NextDNS_set_default_profile(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _NextDNS_add_profile(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _NextDNS_remove_profile(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _NextDNS_write_to_config_file(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);

amxd_status_t _NextDNS_read_from_config_file(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret);


/**
 * @brief Saves the NextDNS Datamodel into a file.
 * 
 * @param object    NextDNS called object
 * @param func      RPC method called
 * @param args      Arguments given, expects nothing
 * @param ret       Retain value, expects a boolean.
 * @return amxd_status_t 
 */
amxd_status_t _NextDNS_save(amxd_object_t* object,
                           amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret);


/**
 * @brief Loads the NextDNS Datamodel from a file.
 * 
 * @param object    NextDNS called object
 * @param func      RPC method called
 * @param args      Arguments given, expects nothing
 * @param ret       Retain value, expects a boolean.
 * @return amxd_status_t 
 */
amxd_status_t _NextDNS_load(amxd_object_t* object,
                           amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret);

#ifdef __cplusplus
}
#endif

#endif