#include "nextdns_methods.h"

/**
 * @brief Dummy Datamodel action for read events.
 * 
 * @param object Caller DM object
 * @param param  Parameter on which the action is invoked (NULL for objects)
 * @param reason Reason why the function is called
 * @param args   Action-specific arguments, NULL in that case (read event)
 * @param retval The result of the action needs to be stored here
 * @param priv   Private action data (unused here).
 * @return amxd_status_t 
 */
amxd_status_t dummy(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    const char* fn_name = amxd_function_get_name(func);
    char* obj_path = amxd_object_get_path(object, AMXD_OBJECT_TERMINATE);

    printf("Dummy : Function %s called on %s\n", fn_name, obj_path);
    printf("Provided arguments = \n");
    fflush(stdout);
    amxc_var_dump(args, STDOUT_FILENO);

    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

amxd_status_t _NextDNS_initialize(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    char ret_str[256];

    ccm_status_t status;
    status = ccm_initialize(ret_str);

    if(status == ccm_error_memory)
    {
        strcat(ret_str, "Error : Memory allocation error in profiles init.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    strcat(ret_str, "Initialized.\n");
    amxc_var_set(cstring_t, ret, ret_str);
    return amxd_status_ok;
}

amxd_status_t _NextDNS_set_default_profile(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    char ret_str[256];

    ccm_status_t status;
    char ret_str_ccm[128];
    char profile_id[10];
    const char *arg = GET_CHAR(args, "ProfileId");
    size_t arg_length = strlen(arg);
    strncpy(profile_id, arg, arg_length);
    profile_id[arg_length] = '\0';
    status = ccm_set_default_profile(profile_id, ret_str_ccm);

    if(status == ccm_error_uninitialized)
    {
        strcat(ret_str, "Error : Initialize function not called.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_empty_argument || status == ccm_error_invalid_argument)
    {
        strcat(ret_str, "Error : Invalid argument given.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    strcat(ret_str, ret_str_ccm);

    amxc_var_set(cstring_t, ret, ret_str);
    return amxd_status_ok;
}

amxd_status_t _NextDNS_add_profile(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    char ret_str[256];

    char profile_id[NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT];
    char target[NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT];
    const char *arg_profile = GET_CHAR(args, "ProfileId");
    const char *arg_target = GET_CHAR(args, "Target");
    strncpy(profile_id, arg_profile, NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT-2);
    profile_id[NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT-1] = '\0';
    strncpy(target, arg_target, NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT-2);
    target[NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT-1] = '\0';

    ccm_status_t status;
    char ret_str_ccm[128];
    status = ccm_add_profile(profile_id, target, ret_str_ccm);  

    if(status == ccm_error_uninitialized)
    {
        strcat(ret_str, "Error : Initialize function not called.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_empty_argument || status == ccm_error_invalid_argument)
    {
        strcat(ret_str, "Error : Invalid argument given.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_memory)
    {
        strcat(ret_str, "Error : Memory allocation error in profiles add.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    strcat(ret_str, ret_str_ccm);
    
    amxc_var_set(cstring_t, ret, ret_str);
    return amxd_status_ok;
}

amxd_status_t _NextDNS_remove_profile(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    char ret_str[256];

    char target[NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT];
    const char *arg_target = GET_CHAR(args, "Target");
    strncpy(target, arg_target, NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT-2);
    target[NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT-1] = '\0';

    ccm_status_t status;
    char ret_str_ccm[128];
    status = ccm_remove_profile(target, ret_str_ccm);
    strcat(ret_str_ccm, " returned by function.\n");

    if(status == ccm_error_uninitialized)
    {
        strcat(ret_str, "Error : Initialize function not called.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_empty_argument || status == ccm_error_invalid_argument)
    {
        strcat(ret_str, "Error : Invalid argument given.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_empty_list)
    {
        strcat(ret_str, "Error : The list is empty.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_status_not_found)
    {
        strcat(ret_str, "No profile was found for this target.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    strcat(ret_str, ret_str_ccm);

    amxc_var_set(cstring_t, ret, ret_str);
    return amxd_status_ok;
}

amxd_status_t _NextDNS_write_to_config_file(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret) {
    char ret_str[256];

    ccm_status_t status;
    char ret_str_ccm[128];
    status = ccm_synchronize_file(ret_str_ccm);

    if(status == ccm_error_uninitialized)
    {
        strcat(ret_str, "Error : Initialize function not called.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_io)
    {
        strcat(ret_str, "Error : Couldn't read the configuration file.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_empty_list)
    {
        strcat(ret_str, "Error : No default profile has been set.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    strcat(ret_str, ret_str_ccm);

    amxc_var_set(cstring_t, ret, ret_str);
    nextdns_cli_start();
    return amxd_status_ok;
}

amxd_status_t _NextDNS_read_from_config_file(amxd_object_t* object,
                    amxd_function_t* func,
                    amxc_var_t* args,
                    amxc_var_t* ret)
{
    char ret_str[512];
    char ret_str_ccm[256];
    ccm_status_t status;
    
    status = ccm_get_from_conf_file(ret_str_ccm);

    if(status == ccm_error_uninitialized)
    {
        strcat(ret_str, "Error : Initialize function not called.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    if(status == ccm_error_io)
    {
        strcat(ret_str, "Error : Couldn't read the configuration file.\n");
        amxc_var_set(cstring_t, ret, ret_str);
        return amxd_status_ok; // To change when not debug anymore.
    }

    strcat(ret_str, ret_str_ccm);

    amxc_var_set(cstring_t, ret, ret_str);
    return amxd_status_ok;
}


/**
 * @brief Saves the NextDNS Datamodel into a file.
 * 
 * @param object    NextDNS called object
 * @param func      RPC method called
 * @param args      Arguments given, expects nothing
 * @param ret       Retain value, expects a boolean.
 * @return amxd_status_t 
 */
amxd_status_t _NextDNS_save(amxd_object_t* object,
                           amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret)
{
    amxc_var_set(bool, ret, false);

    const char *filename = NEXTDNS_DM_SAVE_FILE_PATH;
    amxo_parser_t *parser = nextdns_get_parser();
    amxd_dm_t *dm = nextdns_get_dm();
    amxd_object_t *root = amxd_dm_get_root(dm);
    amxo_parser_save_object(parser, filename, root, false);

    // Setting the retained value to signal success.
    amxc_var_set(bool, ret, true);
    printf("NextDNS Datamodel saved under the file %s.\n", filename);

    return amxd_status_ok;
}


/**
 * @brief Loads the NextDNS Datamodel from a file.
 * 
 * @param object    NextDNS called object
 * @param func      RPC method called
 * @param args      Arguments given, expects nothing
 * @param ret       Retain value, expects a boolean.
 * @return amxd_status_t 
 */
amxd_status_t _NextDNS_load(amxd_object_t* object,
                           amxd_function_t* func,
                           amxc_var_t* args,
                           amxc_var_t* ret)
{
    amxd_status_t rv;
    amxc_var_set(bool, ret, false);

    const char *filename = NEXTDNS_DM_SAVE_FILE_PATH;
    amxo_parser_t *parser = nextdns_get_parser();
    amxd_dm_t *dm = nextdns_get_dm();
    amxd_object_t *root = amxd_dm_get_root(dm);
    
    // Restore the data model to default config
    nextdns_remove_all_profiles_instances();

    // Load the dm from a file.
    rv = amxo_parser_parse_file(parser, filename, root);
    if(rv == amxd_status_ok)
    {
        printf("Succesfully loaded file '%s'.\n", filename);
        amxc_var_set(bool, ret, true);
    }
    else if(rv == amxd_status_object_not_found)
    {
        printf("Error : Save file '%s' not found.\n", filename);
        amxc_var_set(bool, ret, false);
    }
    else
    {
        printf("Error %d during load.\n", rv);
        amxc_var_set(bool, ret, false);
    }
    return rv;
}