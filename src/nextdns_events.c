#include "nextdns_events.h"
#include "nextdns_utils.h"

/**
 * @brief Debug event printer function.
 * 
 * @param sig_name  Type of signal triggered (e.g. "dm:object-changed")
 * @param data      Event data
 * @param priv      Optional extra data
 */
void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv) {
    printf("\nPRINT EVENT\n");
    printf("Signal received - %s\n", sig_name);
    printf("Signal data = \n");
    fflush(stdout);
    if(!amxc_var_is_null(data)) {
        amxc_var_dump(data, STDOUT_FILENO);
    }
}

/**
 * @brief Triggers when the URL parameter of the NextDNS data model is changed.
 */
void _on_enable_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv ) {
    amxc_var_t params;
    bool enable_dm;
    amxd_object_t *nextdns_obj;

    // Get the new value of Enable
    nextdns_obj = nextdns_get_object();
    amxd_object_get_params(nextdns_obj, &params, amxd_dm_access_protected);
    enable_dm = GET_BOOL(&params, "Enable");

    // Start / stop the service accordingly
    if(enable_dm)
    {
        nextdns_cli_start();
        printf("NextDNS CLI has been started.\n");
    }
    else
    {
        nextdns_cli_stop();
        printf("NextDNS CLI has been stopped.\n");
    }
}

/**
 * @brief Triggers when the DefaultProfileId parameter of the NextDNS data model is changed.
 */
void _on_default_profile_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv ) {
    char ret[256]; //unused for now
    const char *profile;
    amxc_var_t params;

    // Retrieve the new value
    nextdns_get_params(&params);
    profile = GET_CHAR(&params, "DefaultProfileId");
    
    // Validate the new value : TODO

    ccm_set_default_profile(profile, ret);
    ccm_synchronize_file(ret);
    printf("nextdns.conf has been updated.\n");
}

/**
 * @brief Triggers when a new instance of the "NextDNS.Profiles." data model object is added.
 */
void _on_profile_added(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv ) {
    update_profiles_list();
}

/**
 * @brief Triggers when any parameter of any instance of the "NextDNS.Profiles." data model object is modified.
 */
void _on_profile_changed(const char* const sig_name,
                  const amxc_var_t* const data,
                  UNUSED void* const priv ) {
    update_profiles_list();
}

void update_profiles_list()
{
    printf("nextdns.conf has been updated.\n");
    int i;
    const char *profile_dm, *target_dm;
    amxc_var_t params;
    char ret[256]; //Unused here

    // Remove existing profiles from memory
    ccm_flush_profiles();

    // Retrieve new profiles from the NextDNS data model
    amxd_object_t *profiles_obj = nextdns_get_profiles();
    for(i = 0; i < (int)nextdns_get_profile_count(); i++)
    {
        amxd_object_t *instance = amxd_object_get_instance(profiles_obj, NULL, i+1);
        amxd_object_get_params(instance, &params, amxd_dm_access_protected);
        profile_dm = GET_CHAR(&params, "ProfileId");
        target_dm = GET_CHAR(&params, "Target");
        ccm_add_profile(profile_dm, target_dm, ret);
    }

    // Write them into the NextDNS configuration file.
    ccm_synchronize_file(ret);
}