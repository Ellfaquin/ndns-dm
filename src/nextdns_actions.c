#include "nextdns_actions.h"

amxd_status_t _dummy_action(amxd_object_t* object,
                       amxd_param_t* param,
                       amxd_action_t reason,
                       const amxc_var_t* const args,
                       amxc_var_t* const retval,
                       void* priv) {
    amxd_status_t status = amxd_status_unknown_error;

    printf("\nDummy action called.\n");

    printf("Reason: ");
    switch(reason)
    {
        case action_param_describe:
            printf("Describe (param)");
            break;
        case action_param_destroy:
            printf("Destroy (param)");
            break;
        case action_param_validate:
            printf("Validate (param)");
            break;
        case action_param_read:
            printf("Read (param)");
            break;
        case action_param_write:
            printf("Write (param)");
            break;
        case action_object_add_inst:
            printf("Add instance (object)");
            break;
        case action_object_del_inst:
            printf("Remove instance (object)");
            break;
        case action_object_describe:
            printf("Describe (object)");
            break;
        case action_object_read:
            printf("Read (object)");
            break;
        case action_object_write:
            printf("Write (object)");
            break;
        case action_object_validate:
            printf("Validate (object)");
            break;
        default:
            printf("Other (%d)", (int)reason);
            break;
    }
    printf("\n");

    if(param == NULL) printf("Not a parameter.\n");
    else printf("Parameter name : %s\n", param->name);

    printf("Provided parameter of type %d", (int)(args->type_id));

    status = amxd_status_ok;
    return status;
}
