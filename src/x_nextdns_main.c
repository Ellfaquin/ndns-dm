#include "nextdns.h"

typedef struct _nextdns_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
} nextdns_app_t;

static nextdns_app_t nextdns;

amxd_dm_t* nextdns_get_dm(void) {
    return nextdns.dm;
}

amxo_parser_t* nextdns_get_parser(void) {
    return nextdns.parser;
}

/**
 * @brief Saves the NextDNS Datamodel.
 */
amxd_status_t NextDNS_save()
{
    const char *filename = NEXTDNS_DM_SAVE_FILE_PATH;
    amxo_parser_t *parser = nextdns_get_parser();
    amxd_dm_t *dm = nextdns_get_dm();
    amxd_object_t *root = amxd_dm_get_root(dm);
    amxo_parser_save_object(parser, filename, root, false);

    // Setting the retained value to signal success.
    printf("NextDNS Datamodel saved under the file %s.\n", filename);

    return amxd_status_ok;
}

/**
 * @brief Loads the NextDNS Datamodel from the save file.
 */
amxd_status_t NextDNS_load()
{
    amxd_status_t rv;
    const char *filename = NEXTDNS_DM_SAVE_FILE_PATH;
    amxo_parser_t *parser = nextdns_get_parser();
    amxd_dm_t *dm = nextdns_get_dm();
    amxd_object_t *root = amxd_dm_get_root(dm);
    
    // Restore the data model to default config
    nextdns_remove_all_profiles_instances();

    // Load the dm from a file.
    rv = amxo_parser_parse_file(parser, filename, root);
    if(rv == amxd_status_ok)
    {
        printf("Succesfully loaded file '%s'.\n", filename);
    }
    else if(rv == amxd_status_object_not_found)
    {
        printf("Error : Save file '%s' not found, going with default config.\n", filename);
    }
    else
    {
        printf("Error %d during load.\n", rv);
    }
    return rv;
}

/**
 * @brief Main function, called at binary starup.
 * 
 * @param reason    Start (0) or stop (1)
 * @param dm        Datamodel
 * @param parser    Parser associated with the datamodel
 * @return int 
 */
int _x_nextdns_main(int reason,
                    amxd_dm_t* dm,
                    amxo_parser_t* parser) {
    char ret[256]; // Unused in this function.
    switch(reason) {
    case 0:     // START
        nextdns.dm = dm;
        nextdns.parser = parser;
        nextdns_cli_start();
        printf("\n/*------------\nNEXTDNS DM STARTED\n------------*/\n");
        ccm_initialize(ret);
        NextDNS_load();
        break;
    case 1:     // STOP
        NextDNS_save();
        nextdns.dm = NULL;
        nextdns.parser = NULL;
        printf("\n/*------------\nNEXTDNS DM STOPPED\n------------*/\n");
        
        break;
    }

    return 0;
}