#include "nextdns_utils.h"

/**
 * @brief Retrieves the NextDNS Data model object.
 */
amxd_object_t* nextdns_get_object()
{
    amxd_dm_t *dm = nextdns_get_dm();
    amxd_object_t *root = amxd_dm_get_root(dm);
    return  amxd_object_get(root, "NextDNS");
}

/**
 * @brief Retrieves parameters (depth 1) of the NextDNS data model.
 */
amxd_status_t nextdns_get_params(amxc_var_t *params)
{
    amxd_object_t* nextdns_obj = nextdns_get_object(); 
    amxd_object_get_params(nextdns_obj, params, amxd_dm_access_protected);
    return amxd_status_ok;
}

/**
 * @brief Retrieves the Profiles object of the NextDNS data model.
 */
amxd_object_t* nextdns_get_profiles()
{
    amxd_object_t* nextdns_obj = nextdns_get_object(); 
    amxd_object_t* profiles_obj = amxd_object_get(nextdns_obj, "Profiles");
    return profiles_obj;
}

uint32_t nextdns_get_profile_count()
{
    amxd_object_t* profile_obj = nextdns_get_profiles();
    return amxd_object_get_instance_count(profile_obj);
}


/**
 * @brief Removes all Profiles instances from the NextDNS data model.
 * 
 */
void nextdns_remove_all_profiles_instances()
{
    int i, j, profiles_count;
    amxd_object_t *instance = NULL;

    // Retrieve new profiles from the NextDNS data model
    amxd_object_t *profiles_obj = nextdns_get_profiles();
    profiles_count = (int)nextdns_get_profile_count();
    for(i = 0; i < profiles_count; i++)
    {
        j = i+1;
        while(instance == NULL)
        {
            instance = amxd_object_get_instance(profiles_obj, NULL, j);
            j++;
        }
        amxd_object_free(&instance);
        instance = NULL;
    }
}

/**
 * @brief Adds an instance of the Profiles object to the NextDNS data model.
 * 
 * @param profile_id    
 * @param target        IPv4/v6 or MAC address
 */
void nextdns_add_profiles_instance(char *profile_id, char *target)
{
    amxd_object_t *instance = NULL;
    amxc_var_t instance_params;
    amxc_var_t profile_number_of_entries;

    // Define the template parameters
    amxc_var_init(&instance_params);
    amxc_var_set_type(&instance_params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &instance_params, "Target", target);
    amxc_var_add_key(cstring_t, &instance_params, "ProfileId", profile_id);

    // Add the instance
    amxd_object_t *profiles_obj = nextdns_get_profiles();
    amxd_object_add_instance(&instance, profiles_obj, NULL, 0, &instance_params);

    // Modify the count parameter
    amxd_object_t *nextdns_obj = nextdns_get_object();
    amxc_var_init(&profile_number_of_entries);
    amxc_var_set(uint32_t, &profile_number_of_entries, nextdns_get_profile_count());
    amxd_object_set_param(nextdns_obj, "ProfileNumberOfEntries", &profile_number_of_entries);

}

/**
 * @brief Sets the DefaultProfileId parameter of the NextDNS data model.
 * 
 * @param profile_id 
 */
void nextdns_set_default_profile_dm(char *profile_id)
{
    amxc_var_t profile_id_var;
    amxd_object_t* nextdns_obj = nextdns_get_object();

    amxc_var_init(&profile_id_var);
    amxc_var_set(cstring_t, &profile_id_var, profile_id);
    amxd_object_set_param(nextdns_obj, "DefaultProfileId", &profile_id_var);

}