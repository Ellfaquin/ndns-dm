#include "client_config_manager.h"

const char *NEXTDNS_CONF_PATH_FILE = "/etc/nextdns.conf";
const int NEXTDNS_CONF_MAX_LINE_LENGTH = 100;
const int NEXTDNS_CONF_MAX_LINE_COUNT = 200;
const size_t NEXTDNS_CONF_MAX_PROFILES_COUNT = 100;
const size_t NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT = 10;
const size_t NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT = 40; // IPv6 : 39 characters

profile_list_t profiles;

/**
 * @brief Initializes the "profiles" variables.
 */
ccm_status_t ccm_initialize(char *ret)
{
    profiles.first = NULL;
    profiles.count = 0;
    profiles.default_profile_id = (char*)malloc(sizeof(char) * NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT);
    if(profiles.default_profile_id == NULL) return ccm_error_memory;
    strncpy(profiles.default_profile_id, "\0", 1);

    return ccm_status_ok;
}

/**
 * @brief Sets the "profile" parameter of nextdns.conf.
 * 
 * @param profile_id 6 characters expected
 */
ccm_status_t ccm_set_default_profile(const char *profile_id, char *ret)
{
    printf("\nSet Default Profile function started.\n");
    if(profile_id == NULL) return ccm_error_empty_argument;
    size_t profile_length = strlen(profile_id);
    if(profile_length == 0 || profile_length > NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT) return ccm_error_invalid_argument;
    if(profiles.default_profile_id == NULL) return ccm_error_uninitialized;
    
    strncpy(profiles.default_profile_id, profile_id, profile_length);
    profiles.default_profile_id[profile_length] = '\0';
    strncpy(ret, profiles.default_profile_id, strlen(profiles.default_profile_id));
    ret[strlen(profiles.default_profile_id)] = '\0';

    return ccm_status_ok;
}

/**
 * @brief Adds a new conditional profile to the profiles list.
 * 
 * @param target    MAC (e.g. 00:1c:42:2e:60:4a) or IP (e.g. 10.0.4.0/24) of the target device
 * @param profile_id NextDNS profile ID
 */
ccm_status_t ccm_add_profile(const char *profile_id, const char *target, char *ret)
{
    if(profile_id == NULL || target == NULL) 
        return ccm_error_empty_argument;
    
    size_t profile_length = strlen(profile_id);
    size_t target_length = strlen(target);
    if(profile_length == 0 || profile_length > NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT-1 
    || target_length == 0 || target_length > NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT-1) 
        return ccm_error_invalid_argument;

    if(profiles.default_profile_id == NULL) return ccm_error_uninitialized;

    profile_t* new_profile = (profile_t*)malloc(sizeof(profile_t));

    new_profile->profile_id = (char*)malloc(sizeof(char) * NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT);
    if(new_profile->profile_id == NULL) return ccm_error_memory;
    new_profile->target = (char*)malloc(sizeof(char) * NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT);
    if(new_profile->target == NULL) return ccm_error_memory;
    strncpy(new_profile->profile_id, profile_id, profile_length);
    strncpy(new_profile->target, target, target_length);
    new_profile->profile_id[profile_length] = '\0';
    new_profile->target[target_length] = '\0';
    
    new_profile->next = profiles.first;
    profiles.first = new_profile;
    profiles.count += 1;

    sprintf(ret, "Profile added : %s ; target %s.\n", profiles.first->profile_id, profiles.first->target);

    return ccm_status_ok;
}

/**
 * @brief Removes a profile from the profiles list.
 * The target is used because profile_ids are not unique keys.
 * @param target MAC address or IP subnet / address.
 */
ccm_status_t ccm_remove_profile(const char *target, char *ret)
{
    if(profiles.default_profile_id == NULL) return ccm_error_uninitialized;
    if(target == NULL) 
        return ccm_error_empty_argument;

    size_t target_length = strlen(target);

    if(target_length == 0 || target_length > NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT )
        return ccm_error_invalid_argument;
        
    if(profiles.first == NULL) return ccm_error_empty_list;

    profile_t *current_profile = profiles.first;

    if(strncmp(current_profile->target, target, target_length) == 0)
    {
        sprintf(ret, "Removed profile %s (target %s).\n", current_profile->profile_id, current_profile->target);
        profiles.first = current_profile->next;
        ccm_free_profile(current_profile);
        profiles.count -= 1;
        return ccm_status_ok;
    }

    while(current_profile->next != NULL)
    {
        if(strncmp(target, current_profile->next->target, target_length) == 0)
        {
            sprintf(ret, "Removed profile %s (target %s).\n", current_profile->next->profile_id, current_profile->next->target);
            ccm_free_profile(current_profile->next);
            current_profile->next = current_profile->next->next;
            profiles.count -= 1;
            return ccm_status_ok;
        }
        current_profile = current_profile->next;
    }

    sprintf(ret, "Profile not found.\n");
    return ccm_status_not_found;   
}

/**
 * @brief Synchronizes the "profiles" variable with the nextdns.conf file.
 * 
 */
ccm_status_t ccm_synchronize_file(char *ret)
{
    if(profiles.default_profile_id == NULL) return ccm_error_uninitialized;
    if(strcmp(profiles.default_profile_id, "\0") == 0) return ccm_error_empty_list;

    FILE *fp = fopen(NEXTDNS_CONF_PATH_FILE, "r");
    if(fp == NULL) return ccm_error_io;

    // Read the file into a buffer
    __uint8_t i = 0;
    char file_lines[NEXTDNS_CONF_MAX_LINE_COUNT][NEXTDNS_CONF_MAX_LINE_LENGTH];
    while (fgets(file_lines[i], 256, fp)) {
        i++;
    }
    fclose(fp);
    
    // Write the modified buffer into the file
    fp = fopen(NEXTDNS_CONF_PATH_FILE, "w");
    if(fp == NULL) return ccm_error_io;
    for(int j = 0; j < i; j++)
    {
        // Re-write the lines that do not concern profiles.
        if (!strncmp(file_lines[j], "profile", 7) == 0) {
            fputs(file_lines[j], fp);
        }
    }

    // Conditional profiles
    profile_t *current_profile = profiles.first;
    while(current_profile != NULL)
    {
        char new_line[NEXTDNS_CONF_MAX_LINE_LENGTH];
        sprintf(new_line, "profile %s=%s\n", current_profile->target, current_profile->profile_id);
        fputs(new_line, fp);
        current_profile = current_profile->next;
    }

    if(strlen(profiles.default_profile_id) > 0)
    {
        char new_line[NEXTDNS_CONF_MAX_LINE_LENGTH];
        sprintf(new_line, "profile %s\n", profiles.default_profile_id);
        fputs(new_line, fp);
    }

    sprintf(ret, "nextdns.conf file has been written.\n");

    fclose(fp);
    return ccm_status_ok;
}

/**
 * @brief Frees the memory of a "profile_t" variable.
 * 
 * @param profile Profile to free.
 */
void ccm_free_profile(profile_t *profile)
{
    if(profile->profile_id != NULL) free(profile->profile_id);
    if(profile->target != NULL) free(profile->target);
    free(profile);
}

/**
 * @brief Removes all "profile_t" objects from the profiles list and frees their memory.
 * 
 * @return ccm_status_t 
 */
ccm_status_t ccm_flush_profiles()
{
    if(profiles.default_profile_id == NULL) return ccm_error_uninitialized;
    if(strcmp(profiles.default_profile_id, "\0") == 0) return ccm_error_empty_list;
    if(profiles.first == NULL) return ccm_status_ok;

    profile_t *current_profile = profiles.first;
    profile_t *next_profile;

    while(current_profile->next != NULL)
    {
        printf("Flush a new profile\n");
        next_profile = current_profile->next->next;
        ccm_free_profile(current_profile->next);
        current_profile->next = next_profile;
    }

    ccm_free_profile(current_profile);
    profiles.first=NULL;

    return ccm_status_ok;
}

ccm_status_t ccm_get_from_conf_file(char *ret)
{
    if(profiles.default_profile_id == NULL) return ccm_error_uninitialized;
    
    ccm_flush_profiles();

    FILE *fp = fopen(NEXTDNS_CONF_PATH_FILE, "r");
    if(fp == NULL) return ccm_error_io;

    char line[NEXTDNS_CONF_MAX_LINE_LENGTH];
    char tmp_target[NEXTDNS_CONF_MAX_TARGET_CHARACTERS_COUNT];
    char tmp_profile[NEXTDNS_CONF_MAX_PROFILE_CHARACTERS_COUNT];

    char ret_tmp[128];

    nextdns_remove_all_profiles_instances();
    sprintf(ret, "Removed existing profiles.\n");

    while (fgets(line, sizeof(line), fp) != NULL) {
        tmp_profile[0] = '\0';
        tmp_target[0] = '\0';
        size_t len = strlen(line);
        if (len > 0 && line[len - 1] == '\n') {
            line[len - 1] = '\0';
        }

        // Conditional profiles
        if (strchr(line, '=') != NULL) {
            strcat(ret, ret_tmp);
            if (sscanf(line, "profile %63[^=]=%63s", tmp_target, tmp_profile) == 2) {
                ccm_add_profile(tmp_profile, tmp_target, ret_tmp);
                nextdns_add_profiles_instance(tmp_profile, tmp_target);
                sprintf(ret_tmp, "Added profile %s=%s\n", tmp_target, tmp_profile);
                strcat(ret, ret_tmp);
            }
        }

        // Default profile
        else
        {
            if (sscanf(line, "profile %127s", tmp_profile) == 1) {
                ccm_set_default_profile(tmp_profile, ret_tmp);
                nextdns_set_default_profile_dm(tmp_profile);
                sprintf(ret_tmp, "Default profile set to %s\n", tmp_profile);
                strcat(ret, ret_tmp);
            }
        }
    }
    fclose(fp);

    return ccm_status_ok;
}