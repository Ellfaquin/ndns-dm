LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "git://https://gitlab.com/Ellfaquin/ndns-dm;protocol=https;branch=main"

PV = "1.0+git${SRCPV}"
SRCREV = "3f5e776d1746a6e6a3f418b33fc00289fd54d348"

S = "${WORKDIR}/git"

DEPENDS += "openssl"
DEPENDS += "libamxb"
DEPENDS += "libamxc"
DEPENDS += "libamxd"
DEPENDS += "libamxp"
DEPENDS += "libamxo"

RDEPENDS:${PN} += "libamxb"
RDEPENDS:${PN} += "libamxc"
RDEPENDS:${PN} += "libamxd"
RDEPENDS:${PN} += "libamxp"
RDEPENDS:${PN} += "libamxo"
RDEPENDS:${PN} += "bash"

EXTRA_OEMAKE += "DEST=${D}"

FILES:${PN} += "/usr/lib/amx/x_nextdns/"
FILES:${PN} += "/usr/bin/x_nextdns"
FILES:${PN} += "/etc/amx/x_nextdns"
FILES:${PN} += "/usr/scripts/x_nextdns"

inherit update-rc.d
INITSCRIPT_NAME = "startup_script"
INITSCRIPT_PARAMS = "start 99 2 3 4 5 . stop 10 0 1 6 ."

do_configure () {
        # Specify any needed configure commands here
        :
}

do_compile () {
        # You will almost certainly need to add additional arguments here
        oe_runmake
}

do_install () {
        # This is a guess; additional arguments may be required
        oe_runmake install
}
